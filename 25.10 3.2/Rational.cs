﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _25._10_3._2
{
    class Rational
    {
        public int _p;
        public int _q;

        public Rational(int p, int q)
        {
            if (q < 0)
                p = 0;
            if (p % 1 != p)
                p = 0;
            _p = p;
            _q = q;
        }

        public bool GreaterThan (Rational rational1)
        {
            if (rational1>this)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator > (Rational rational1, Rational rational2)
        {
            if ((rational1._p) * (rational2._q) > (rational1._q) * (rational2._p))
            {
                return true;
            }
            else
            {
                return false;
            }         
        }

        public static bool operator <(Rational rational1, Rational rational2)
        {
            if ((rational1._p) * (rational2._q) < (rational1._q) * (rational2._p))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool Equals (Rational rational1, Rational rational2)
        {
            if (rational1 == rational2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator == (Rational rational1, Rational rational2)
        {
            if ((rational1._p) * (rational2._q) == (rational1._q) * (rational2._p))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(Rational rational1, Rational rational2)
        {
            if ((rational1._p) * (rational2._q) != (rational1._q) * (rational2._p))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static Rational operator + (Rational rational1, Rational rational2)
        {
            Rational result = new Rational((rational1._p) * (rational2._q) + (rational1._q) * (rational2._p) , (rational1._q) * (rational2._q));
            return result;
        }

        public static Rational operator - (Rational rational1, Rational rational2)
        {
            Rational result = new Rational((rational1._p) * (rational2._q) - (rational1._q) * (rational2._p), (rational1._q) * (rational2._q));
            return result;
        }

        public static Rational operator * (Rational rational1, Rational rational2)
        {
            Rational result = new Rational((rational1._p) * (rational2._p), (rational1._q) * (rational2._q));
            return result;
        }

        public int getNumerator(Rational rational)
        {
            return rational._p;
        }

        public int getDenominator(Rational rational)
        {
            return rational._q;
        }

        public override string ToString()
        {
            return $"{_p} + / + {_q}";
        }
    }
}
