﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _25._10_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Rational rational1 = new Rational(2, 3);
            Rational rational2 = new Rational(4, 5);
            Console.WriteLine(rational1.GreaterThan(rational2));
        }
    }
}
